I put this together based on the following site: https://www.digitalocean.com/community/tutorials/how-to-use-traefik-v2-as-a-reverse-proxy-for-docker-containers-on-ubuntu-20-04

This stands up a basic Traefik reverse proxy with file support that will dynamically pick up Docker containers setup on the same machine

This assumes that your A record is already pointing to `traefik.seattlematrix.org`

It also presumes that `docker` and `docker-compose` are already installed on the machine.

It also presumes you've created a docker network like so `docker network create web`.

Copy the `acme.json.example` to `acme.json`.

You'll want to do a `sudo chmod 0600 acme.json` in order for the permissions to be set correctly to automatically pull LetsEncrypt certs

Add admin credentials as usernames and passwords in the format `username:password-hash` as hashed by `htpasswd` in `users.secret`.
